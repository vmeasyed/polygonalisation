package plugins.vannary.polygonalisation;


import icy.image.IcyBufferedImage;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import plugins.adufour.opencv.OpenCV;
import plugins.adufour.roi.LabelExtractor;
import plugins.kernel.roi.roi2d.ROI2DPolygon;

public class Utils {

	public static List<ROI2DPolygon> findContours(final Sequence seqBin, boolean hole) {

	
		OpenCV.initialize();

		final Mat image = OpenCV.convertToOpenCV(seqBin.getFirstImage());

		
		final List<MatOfPoint> contours = new ArrayList<MatOfPoint>();


		final Mat hierarchy = new Mat();
		
	
		Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_CCOMP,
				Imgproc.CHAIN_APPROX_NONE);//.CHAIN_APPROX_TC89_KCOS) // meilleure, mais pas regul
		//RETR_LIST: retrieves all of the contours without establishing any hierarchical relationships

	    /*CV_RETR_EXTERNAL retrieves only the extreme outer contours. It sets hierarchy[i][2]=hierarchy[i][3]=-1 for all the contours.
	    CV_RETR_LIST retrieves all of the contours without establishing any hierarchical relationships.
	    CV_RETR_CCOMP retrieves all of the contours and organizes them into a two-level hierarchy. At the top level, there are external boundaries of the components. At the second level, there are boundaries of the holes. If there is another contour inside a hole of a connected component, it is still put at the top level.
	    CV_RETR_TREE retrieves all of the contours and reconstructs a full hierarchy of nested contours. This full hierarchy is built and shown in the OpenCV contours.c demo.*/

		final List<ROI2DPolygon> rois2D = new ArrayList<ROI2DPolygon>();

		
		for (int i = 0; i < contours.size(); i++) {
			// if not a hole
			if ( hierarchy.get(0, i)[3] == -1 ) {
				final Point[] pts = contours.get(i).toArray();

				final List<Point2D> points = new ArrayList<Point2D>();

				for (int j = 0; j < pts.length; j++) {
					points.add(new Point2D.Double(pts[j].x, pts[j].y));
				}
				
			final ROI2DPolygon roi = new ROI2DPolygon(points); 
			rois2D.add(roi);
			}
			else {
				if (hole){
				final Point[] pts = contours.get(i).toArray();

				final List<Point2D> points = new ArrayList<Point2D>();

				for (int j = 0; j < pts.length; j++) {
					points.add(new Point2D.Double(pts[j].x, pts[j].y));
				}
				final ROI2DPolygon roi = new ROI2DPolygon(points); 
				roi.setColor(Color.RED);
				roi.setName("hole");
				rois2D.add(roi);
				}
			}			

		}

		return rois2D;
	}
	
	/*// Canny
	public static List<ROI2DPolygon> findContoursCanny(final Sequence seq, int value) {

		//TODO a tester
		 /// Detect edges using canny
		 // Canny( src_gray, canny_output, thresh, thresh*2, 3 );
		  /// Find contours
		  // findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
				
		
		OpenCV.initialize();

		final Mat image = OpenCV.convertToOpenCV(seq.getFirstImage());

		
		final List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

		final Mat canny_output=image;
		double thresh = 100;
		Imgproc.Canny( canny_output, image,  thresh, thresh*2, 3, false );
		
		final Mat hierarchy = new Mat();
		Imgproc.findContours(canny_output, contours, hierarchy, Imgproc.RETR_CCOMP,
				Imgproc.CHAIN_APPROX_NONE);//.CHAIN_APPROX_TC89_KCOS) // meilleure, mais pas regu


		final List<ROI2DPolygon> rois2D = new ArrayList<ROI2DPolygon>();

		
		for (int i = 0; i < contours.size(); i++) {
			// if not a hole
			if ( hierarchy.get(0, i)[3] == -1 ) {
			final Point[] pts = contours.get(i).toArray();

			final List<Point2D> points = new ArrayList<Point2D>();

			
			for (int j = 0; j < pts.length; j++) {
				points.add(new Point2D.Double(pts[j].x, pts[j].y));
			}

			final ROI2DPolygon roi = new ROI2DPolygon(points); 
			
			rois2D.add(roi);
			}

		}

		return rois2D;
	}
	*/

	
	
	
	
}
