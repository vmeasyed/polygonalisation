package plugins.vannary.polygonalisation;

import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import icy.image.IcyBufferedImage;
import icy.main.Icy;
import icy.roi.BooleanMask2D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.sequence.Sequence;
import icy.system.IcyHandledException;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.connectedcomponents.ConnectedComponent;
import plugins.adufour.connectedcomponents.ConnectedComponents;
import plugins.adufour.connectedcomponents.ConnectedComponents.ExtractionType;
import plugins.adufour.ezplug.EzGroup;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarDouble;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.ezplug.EzVarText;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DPolygon;

public class Polygonalisation extends EzPlug implements Block, EzStoppable {

	private EzVarSequence input;
	private EzVarDouble valPolyg;
	private EzVarInteger minSizeValue;
	private EzVarInteger maxSizeValue;
	private EzVarText colorTxt;
	private VarROIArray outROI;
	private static EzVarBoolean hole = new EzVarBoolean("Show holes ", true);
	protected static EzVarBoolean discardEdges = new EzVarBoolean("Remove border objects ", true);

	@Override
	protected void initialize() {
		input = new EzVarSequence("Input");
		input.setToolTipText("Input sequence");
		valPolyg = new EzVarDouble("Min line deviation", 2, 0, 25, .1);
		colorTxt = new EzVarText("Target color intensity", null, true);
		colorTxt.setToolTipText("Intensity value of the regions to polygonize. If multiple channels, separate by space.");


		addEzComponent(input);
		addEzComponent(valPolyg);
		addEzComponent(colorTxt);
		addEzComponent(hole);
		addEzComponent(discardEdges);


		minSizeValue = new EzVarInteger("Min Size", 120, 0, 15000, 1);
		minSizeValue.setToolTipText("Minimum size of the polygonized regions");
		maxSizeValue = new EzVarInteger("Max size", 1500000, 0, 540500000, 10);
		maxSizeValue.setToolTipText("Maximum size of the polygonized regions");

		EzGroup paramSizeVar = new EzGroup("Roi Size", minSizeValue, maxSizeValue);
		addEzComponent(paramSizeVar);
	}

	@Override
	public void declareInput(VarList inputMap) {
		input = new EzVarSequence("Sequence");
		valPolyg = new EzVarDouble("Value", 2, 0, 25, .1);
		colorTxt = new EzVarText("Color intensity value", null, true);
		hole = new EzVarBoolean("Show holes ", true);
		discardEdges = new EzVarBoolean("Remove border objects ", true);

		inputMap.add(input.name, input.getVariable());
		inputMap.add(valPolyg.name, valPolyg.getVariable());
		inputMap.add(colorTxt.name, colorTxt.getVariable());

		inputMap.add(hole.name, hole.getVariable());
		inputMap.add(discardEdges.name, discardEdges.getVariable());

		minSizeValue = new EzVarInteger("Min size value", 120, 0, 15000, 1);
		maxSizeValue = new EzVarInteger("Max size value", 11150000, 0, 155550000, 1);
		inputMap.add(minSizeValue.name, minSizeValue.getVariable());
		inputMap.add(maxSizeValue.name, maxSizeValue.getVariable());
	}

	@Override
	public void declareOutput(VarList outputMap) {
		outROI = new VarROIArray("Extracted ROI");
		outputMap.add("ROI", outROI);
	}

	@Override
	protected void execute() {
		final Sequence seq = input.getValue(true);
		double min_dev = valPolyg.getValue(true);
		String couleur = colorTxt.getValue(true);

		int[] vals;
		try {
			vals = Arrays.stream(couleur.split(" ")).mapToInt(s -> Integer.parseInt(s)).toArray();
		} catch (Exception e) {
			throw new IcyHandledException(e.getMessage());
		}
		if (vals.length != seq.getSizeC())
			throw new IcyHandledException(vals.length + " intensity values instead of " + seq.getSizeC());

		try {
			process(seq, vals, minSizeValue.getValue(), maxSizeValue.getValue(), min_dev);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new IcyHandledException("Interrupted polygonalization");
		}

		List<ROI> rois = seq.getROIs();
		if (rois.isEmpty())
			throw new IcyHandledException("No roi was created");

		if (isHeadLess())
			outROI.setValue(rois.toArray(new ROI[rois.size()]));
	}

	@Override
	public void clean() {
	}

	// openCV to extract chain contour + polygonal approximation of Rosin
	// to avoid too much control points
	public static void process(Sequence seq, int[] cvalue, int minValue, int maxValue, double min_dev)
			throws InterruptedException {

		Sequence seqBin = new Sequence();
		seqBin.setImage(0, 0, new IcyBufferedImage(seq.getWidth(), seq.getHeight(), 1, DataType.UBYTE));

		IcyBufferedImage bin = seqBin.getImage(0, 0);
		double[] tabOut = Array1DUtil.arrayToDoubleArray(bin.getDataXY(0), bin.isSignedDataType());
		int w = seq.getWidth();
		int h = seq.getHeight();
		IcyBufferedImage imaIn = seq.getFirstImage();

		int WHITE = 255;
		if (seq.getSizeC() == 1) {
			double[] tabIn = Array1DUtil.arrayToDoubleArray(imaIn.getDataXY(0), imaIn.isSignedDataType());

			for (int x = 0; x < w; x++)
				for (int y = 0; y < seq.getHeight(); y++) {
					if (Thread.interrupted())
						throw new InterruptedException();
					if (tabIn[x + y * w] == cvalue[0])
						tabOut[x + y * w] = WHITE;
				}
		} else {
			double[] tabInR = Array1DUtil.arrayToDoubleArray(imaIn.getDataXY(0), imaIn.isSignedDataType());
			double[] tabInG = Array1DUtil.arrayToDoubleArray(imaIn.getDataXY(1), imaIn.isSignedDataType());
			double[] tabInB = Array1DUtil.arrayToDoubleArray(imaIn.getDataXY(2), imaIn.isSignedDataType());
			for (int x = 0; x < seq.getWidth(); x++)
				for (int y = 0; y < seq.getHeight(); y++) {
					if (Thread.interrupted())
						throw new InterruptedException();
					if ((tabInR[x + y * w] == cvalue[0]) && (tabInG[x + y * w] == cvalue[1])
							&& (tabInB[x + y * w] == cvalue[2]))
						tabOut[x + y * w] = WHITE;
				}
		}
		// TODO creation de l'image binaire

		bin.setDataXY(0, Array1DUtil.doubleArrayToArray(tabOut, bin.getDataXY(0)));

		// send binary image to contour extraction
		final List<ROI2DPolygon> rois = Utils.findContours(seqBin, hole.getValue());

		int min = 1;
		for (int i = 0; i < rois.size(); i++) {
			final ROI2DPolygon p = rois.get(i);
			final Rectangle bound = p.getBounds();

			if (p.getNumberOfPoints() > minValue && p.getNumberOfPoints() < maxValue) {

				if (!discardEdges.getValue())	{


					Polygon approxPolyg = linearApproximation(p, (float) min_dev);
					ROI2DPolygon roiP = new ROI2DPolygon();
					roiP.setPolygon(approxPolyg);
					roiP.setName(p.getName());
					roiP.setColor(Color.BLUE);
					seq.addROI(roiP);
				}else{// Edge discarded
					if (bound.getMinX() <= min || bound.getMinY() <= min || bound.getMaxX() >= (w - 1)
							|| bound.getMinY() + bound.height + 1 >= (h - 1)) { // onEdgeX = (x == 0 || x == width - 1);
						;

					} else{
						Polygon approxPolyg = linearApproximation(p, (float) min_dev);
						ROI2DPolygon roiP = new ROI2DPolygon();
						roiP.setPolygon(approxPolyg);
						roiP.setName(p.getName());
						roiP.setColor(Color.BLUE);
						seq.addROI(roiP);
					}
				}
			}
		}


	}

	// Aproximation polygonale from Rosin's code lines.c of lines_arcs
	// Rosin's method

	static int RECURSION = 1; /*
	 * compile tail recursion if 1 in function segment
	 */
	static int MAKE_ONE = 1; /* compile heuristic if 1 in function deviation */

	static int SIZE = 50000; /* used to be 2048 - not enough for racer image */
	static double NINETY = 1.570796327;

	// TODO double MIN_DEV=1.9; /* minimum allowable deviation ad hoc value! */

	static int OK = 0;
	static int NULL_BRKPT = -3;
	static int NULL_SIG = -9999;
	static int ALL_SBREAK = 0; /* all non-endpoints as soft breakpoints */
	static int NO_SBREAK = 5; /* no soft breakpoints */

	static int FIRST = 1; /* three constants used in improve_lines */
	static int SECOND = 2;
	static int THIRD = 3;

	// Extract contour EDGE
	ArrayList<CntLabel> listContours = new ArrayList<CntLabel>();

	private static int[][] convert2edget(ROI roi) {

		// int cntSize = (int) roi.getNumberOfContourPoints();
		ROI2DPolygon roiPolyg = (ROI2DPolygon) roi;
		Polygon polyg = roiPolyg.getPolygon();
		int cntSize = polyg.npoints;
		int[][] edget;
		edget = new int[cntSize][3];
		for (int np = 0; np < cntSize; np++) {
			edget[np][0] = polyg.xpoints[np];
			edget[np][1] = polyg.ypoints[np];
		}
		return (edget);
	}

	private static Polygon linearApproximation(ROI2DPolygon roi, float value_dev) {

		int i, j;
		float sig[] = new float[1]; // not really used - just dummy param for
		// segment
		float sum[] = new float[1]; // as for sig above

		int line_start_index;

		long no_pixels, no_lines_1, no_lines_2;
		long no_lines_temp;
		boolean use_min_dev;
		int k, l;
		int nl;

		float sigs[] = new float[SIZE];
		float min_dev; // minimum deviation for a line - default MIN_DEV but can
		// be set by user
		int flags[] = new int[SIZE];

		use_min_dev = false;

		min_dev = (float) value_dev;// MIN_DEV;

		// variables for statistics
		no_pixels = 0;
		no_lines_1 = 0;
		no_lines_2 = 0;

		int[][] edget = null;
		// ArrayList<EdgeRegion> edgeRlist=new ArrayList<EdgeRegion>();

		// Conversion pour la polygonalisation
		edget = convert2edget(roi);

		int number_pixels = edget.length;
		segment(0, number_pixels - 1, sig, sum, edget, sigs, min_dev, flags);

		// super data output format
		line_start_index = 1;
		l = 0;
		// Contour Polygonalise

		Polygon polygon = new Polygon();
		// System.out.println(" edget.length:"+edget.length);
		float polySigs[] = new float[edget.length / 2];
		for (j = 0; j < edget.length; j++) {
			if (flags[j] != NULL_BRKPT) {
				// ctp[nl].pt[l].x = edget[j][0];
				polygon.addPoint(edget[j][0], edget[j][1]);

				// polySigs[l]=sig[0];// DEBUG ???
				l++;
				// System.out.println(" l:"+l+ " px: "+edget[j][0]+"
				// py:"+edget[j][1] + " sig_out: "+sig[0]+ " sigs:"+sigs[j]);
				line_start_index = j;

			}
		}

		// print out statistics

		return (polygon);
	}

	private ArrayList<EdgeRegion> linearApprox(Sequence seq, float value_dev, int value) {
		int i, j;
		float sig[] = new float[1]; // not really used - just dummy param for
		// segment
		float sum[] = new float[1]; // as for sig above

		int line_start_index;

		long no_pixels, no_lines_1, no_lines_2;
		long no_lines_temp;
		boolean use_min_dev;
		int k, l;
		int nl;
		int number_pixels;
		ArrayList<Polygon> edgePolyg = new ArrayList<Polygon>();

		float sigs[] = new float[SIZE];
		float min_dev; // minimum deviation for a line - default MIN_DEV but can
		// be set by user
		int flags[] = new int[SIZE];

		use_min_dev = false;

		min_dev = (float) value_dev;// MIN_DEV;

		// variables for statistics
		no_pixels = 0;
		no_lines_1 = 0;
		no_lines_2 = 0;

		Sequence labeledSequence = new Sequence();
		int minSize = (int) Math.round(minSizeValue.getValue());

		System.out.println("Dection Contour MIN_DEV:" + value_dev + " minSize:" + minSize);

		int maxSize = (int) Math.round(maxSizeValue.getValue());
		// int value = ccValue.getValue();

		List<ConnectedComponent> ccs = ConnectedComponents.extractConnectedComponents(seq, value, ExtractionType.VALUE,
				false, false, false, minSize, maxSize, labeledSequence).get(0);

		/*
		 * List<ROI> cellRois = LabelExtractor.extractLabels(seq,
		 * LabelExtractor.ExtractionType.SPECIFIC_LABEL, value);
		 */
		labeledSequence.setName(" extractEdge");
		// addSequence(labeledSequence);

		listContours = new ArrayList<CntLabel>(); // RAZ
		// extractEdge(labeledSequence.getFirstImage(), ccs );
		extractEdgeO(labeledSequence.getFirstImage(), ccs.size());

		int[][] edget = null;
		ArrayList<EdgeRegion> edgeRlist = new ArrayList<EdgeRegion>();

		// System.out.println(" Approx:"+listContours.size());
		for (nl = 0; nl < listContours.size(); nl++) {
			CntLabel contour = listContours.get(nl);
			edget = new int[contour.pts.size()][3];
			for (int np = 0; np < contour.pts.size(); np++) {
				edget[np][0] = contour.pts.get(np).x;// (int [][]) edge.get(nl);
				edget[np][1] = contour.pts.get(np).y;
			}
			number_pixels = edget.length;
			// number_pixels = ct[nl].nbpts;
			if (number_pixels <= 2) {
				System.out.println(nl + " WARNING: skipping pixel list with only %d pixels\n" + number_pixels);
				continue;
			}
			double px = 0;
			double py = 0;
			// count number of pixels
			no_pixels += number_pixels;
			// System.out.println(" number_pixels:"+number_pixels);
			// ATTENTION : Bornes: 0 -> number_pixels non compris
			for (i = 0; i < number_pixels; i++) {
				sigs[i] = NULL_SIG; // ???
				flags[i] = OK;
				px += edget[i][0];
				py += edget[i][1];
			}

			Point cnt = new Point();
			cnt.x = (int) px / number_pixels;
			cnt.y = (int) py / number_pixels;

			segment(0, number_pixels - 1, sig, sum, edget, sigs, min_dev, flags);

			// count number of lines after first stage
			/*
			 * for (i=1;i<number_pixels;i++) if (flags[i] != NULL_BRKPT)
			 * no_lines_1++;
			 */

			// super data output format
			line_start_index = 1;
			l = 0;
			// Contour Polygonalise

			px = 0;
			py = 0;
			Polygon polygon = new Polygon();
			// System.out.println(" edget.length:"+edget.length);
			float polySigs[] = new float[edget.length / 2];
			for (j = 0; j < edget.length; j++) {
				if (flags[j] != NULL_BRKPT) {
					// ctp[nl].pt[l].x = edget[j][0];
					polygon.addPoint(edget[j][0], edget[j][1]);
					px += edget[j][0];
					py += edget[j][1];
					// polySigs[l]=sig[0];// DEBUG ???
					l++;
					// System.out.println(" l:"+l+ " px: "+edget[j][0]+"
					// py:"+edget[j][1] + " sig_out: "+sig[0]+ "
					// sigs:"+sigs[j]);
					line_start_index = j;

				}
			}

			EdgeRegion edgeOne = new EdgeRegion(polygon, cnt, polySigs);
			edgeRlist.add(edgeOne);
			// ctp[nl].nbpts = l-1;

		}

		// print out statistics
		System.out.println("number of pixels processed:   " + no_pixels);
		System.out.println("after first pass\n");
		System.out.println("number of lines:              " + no_lines_1);

		return (edgeRlist);
	}

/*	private int[][] convertRoi2Segment(ROI2D roi)  {
		// segment = new int[contour.pts.size()][3];
		int nbpts = (int) roi.getNumberOfContourPoints();
		int[][] segment = new int[nbpts][3];
		// for (int i=0; i<listContours.size(); i++) {

		ROI2DPolygon polygon = (ROI2DPolygon) roi;
		for (int np = 0; np < nbpts; np++) {
			segment[np][0] = (int) polygon.getPolygon2D().xpoints[np];
			segment[np][1] = (int) polygon.getPolygon2D().ypoints[np];
		}
		return (segment);
	}
*/
	public static Sequence doMasksFromRoi(Sequence seqIn, List<ROI> rois) throws InterruptedException{

		Sequence seqOut = new Sequence();
		seqOut.setImage(0, 0, new IcyBufferedImage(seqIn.getWidth(), seqIn.getHeight(), 1, DataType.UBYTE));
		IcyBufferedImage bin = seqOut.getImage(0, 0);
		double[] _out = Array1DUtil.arrayToDoubleArray(bin.getDataXY(0), bin.isSignedDataType());

		int w = seqIn.getWidth();
		int h = seqIn.getHeight();

		int WHITE = 255;
		// ArrayList<ROI2D> allRois= seqIn.getROI2Ds();

		for (ROI roi : rois) {
			ROI2D roi2d = (ROI2D) roi;
			BooleanMask2D roiMask = roi2d.getBooleanMask(true);
			int minx = Math.max(0, roiMask.bounds.x);
			int maxx = Math.min(w, roiMask.bounds.x + roiMask.bounds.width);
			int miny = Math.max(0, roiMask.bounds.y);
			int maxy = Math.min(h, roiMask.bounds.y + roiMask.bounds.height);

			for (int x = minx; x < maxx; x++)
				for (int y = miny; y < maxy; y++) {
					if (roiMask.contains(x, y)) {
						_out[x + y * w] = WHITE;

					}
				}
		}
		bin.setDataXY(0, Array1DUtil.doubleArrayToArray(_out, bin.getDataXY(0)));
		// seqOut.addImage(bin);

		seqOut.setName(seqIn.getFilename());

		return (seqOut);
	}

	EdgeRegion polygonal(int[][] edget, Point cnt, float value_dev) {
		float sig[] = new float[1]; // not really used - just dummy param for
		// segment
		float sum[] = new float[1]; // as for sig above

		float sigs[] = new float[SIZE];
		float min_dev; // minimum deviation for a line - default MIN_DEV but can
		// be set by user
		int flags[] = new int[SIZE];

		boolean use_min_dev = false;

		min_dev = value_dev;// MIN_DEV;
		int number_pixels = edget.length;

		segment(0, number_pixels - 1, sig, sum, edget, sigs, min_dev, flags);

		// super data output format

		int line_start_index = 1;
		int l = 0;
		// Contour Polygonalise

		int px = 0;
		int py = 0;
		Polygon polygon = new Polygon();
		// System.out.println(" edget.length:"+edget.length);
		float polySigs[] = new float[edget.length / 2];
		for (int j = 0; j < edget.length; j++) {
			if (flags[j] != NULL_BRKPT) {
				// ctp[nl].pt[l].x = edget[j][0];
				polygon.addPoint(edget[j][0], edget[j][1]);
				px += edget[j][0];
				py += edget[j][1];
				// polySigs[l]=sig[0];// DEBUG ???
				l++;
				// System.out.println(" l:"+l+ " px: "+edget[j][0]+"
				// py:"+edget[j][1] + " sig_out: "+sig[0]+ " sigs:"+sigs[j]);
				line_start_index = j;

			}
		}
		EdgeRegion edgeOne = new EdgeRegion(polygon, cnt, polySigs);

		return (edgeOne);
	}

	static void segment(int start_in, int finish_in, float[] sig_out, float[] sum_out, int[][] ct, float[] sigs,
			float min_dev, int[] flags) {
		int i;
		int pos[] = new int[1];
		;
		float dev[] = new float[1];
		float sig1;// =new float[1];
		float[] sig2 = new float[1];
		float[] sig3 = new float[1];
		float max_sig;
		float[] sum1 = new float[1];
		float[] sum2 = new float[1];
		;
		float[] sum3 = new float[1];
		float best_sum;
		boolean ok[] = new boolean[1];
		double length;
		Point2D cpt2[] = new Point2D.Float[ct.length];
		int end2[] = new int[1];

		// compute significance at this level

		// System.out.println("start_in :"+ start_in+" finish_in :"+finish_in +
		// " end2:"+end2[0] );
		transform(start_in, finish_in, ct, cpt2, end2);
		deviation(pos, dev, ok, sum1, cpt2, end2[0]);

		if (dev[0] == 0)
			dev[0] = min_dev;

		pos[0] = pos[0] + start_in - 1;
		// euclidean length
		length = Math.pow(cpt2[0].getX() - cpt2[end2[0]].getY(), 2);// TODO
		// regarder
		// les
		// limites
		// des
		// tableaux
		// !!!
		length += Math.pow(cpt2[0].getX() - cpt2[end2[0]].getY(), 2);
		length = Math.sqrt(length);
		sig1 = dev[0] / (float) length;
		sum1[0] = sum1[0] / (float) length;
		if (((finish_in - start_in) < 3) || (dev[0] < min_dev) || (ok[0] == false)) {
			// save line match at this lowest of levels
			// modify x_c,y_c data to delete unused coordinates
			// save significance
			sigs[start_in] = sig1;

			sig_out[0] = sig1;
			sum_out[0] = sum1[0];
			// delete breakpoints between end points
			if ((finish_in - start_in) >= 2)
				for (i = start_in + 1; i < finish_in; i++)
					flags[i] = NULL_BRKPT;
		} else {
			// recurse to next level down
			segment(start_in, pos[0], sig2, sum2, ct, sigs, min_dev, flags);
			segment(pos[0], finish_in, sig3, sum3, ct, sigs, min_dev, flags);
			// #if RECURSION
			// get best significance from lower level
			if (sig2[0] < sig3[0]) {
				max_sig = sig2[0];
				best_sum = sum2[0];
			} else {
				max_sig = sig3[0];
				best_sum = sum3[0];
			}
			if (max_sig < sig1) {
				// return best significance, keep lower level description
				sig_out[0] = max_sig;
				sum_out[0] = best_sum;
			} else {
				// line at this level is more significant so remove coords
				// at lower levels
				// printf("JUST REPLACED LOWER LEVEL LINES\n");
				sig_out[0] = sig1;
				sum_out[0] = sum1[0];
				sigs[start_in] = sig_out[0];

				if ((finish_in - start_in) >= 2)
					for (i = start_in + 1; i < finish_in; i++)
						flags[i] = NULL_BRKPT;
			}
			// #endif
		}
		cpt2 = null;
	}

	static void transform(int start, int finish, int[][] ct, Point2D[] cpt2, int[] end2) {
		int i, j;
		int x_offset, y_offset, x_end, y_end; // float
		double angle, sine, cosine;
		double temp;
		float x = 0;
		float y = 0;
		Point2D pt2 = new Point2D.Float(0, 0);

		x_offset = ct[start][0]; // x nl
		y_offset = ct[start][1]; // y nl
		x_end = ct[finish][0]; // x
		y_end = ct[finish][1]; // y nl

		if ((x_end - x_offset) == 0.0) {
			if ((y_end - y_offset) > 0.0)
				angle = -NINETY;
			else
				angle = NINETY;
		} else {
			temp = ((float) (y_end - y_offset) / (float) (x_end - x_offset));
			angle = -Math.atan(temp);
		}
		cosine = Math.cos(angle);
		sine = Math.sin(angle);
		j = 0;
		for (i = start; i < finish; i++) { // <=finish
			// j++;
			pt2.setLocation(ct[i][0] - x_offset, ct[i][1] - y_offset); // x nl

			x = (float) (cosine * pt2.getX()) - (float) (sine * pt2.getY());
			y = (float) (sine * pt2.getX()) + (float) (cosine * pt2.getY());
			// pt.x = temp;
			cpt2[j] = new Point2D.Float();
			cpt2[j].setLocation(x, y);

			j++;
		}
		// System.out.println(" cpt2[j].x: " +cpt2[j-1].getX());
		end2[0] = j - 1;
		// System.out.println(" end2[0]:"+end2[0]);
	}

	private static void deviation(int[] pos, float[] dev, boolean[] ok, float[] sum, Point2D[] cpt2, int end2) {
		int i;
		int pos1;
		float max1, temp; // temp used for abs deviation - dont change!!

		pos1 = 0;
		max1 = 0.0f;
		sum[0] = 0.0f;
		for (i = 0; i < end2; i++) {
			temp = (float) Math.abs(cpt2[i].getY());
			if (temp > max1) {
				max1 = temp;
				pos1 = i;
			}
			sum[0] += temp;
		}
		// if no peak found - signal with ok
		if (max1 == 0.0)
			ok[0] = false;
		else
			ok[0] = true;
		pos[0] = pos1;
		dev[0] = max1;
	}
	//// End approxLine

	private static class EdgeRegion {
		Polygon poly;
		Point center;
		float sigs[];
		int label;

		EdgeRegion(Polygon poly, Point center, float[] sigs) {
			this.poly = poly;
			this.center = center;
			this.sigs = sigs; // for arcline!!!
		}
	}

	private class PointContour {
		private int x;
		private int y;
		private int dir; // freeman

	}

	private class CntLabel {
		private ArrayList<PointContour> pts;
		private int label;
	}

	// Extract contours
	// EDGE
	ArrayList edge = null; // edge[compteur list][0=X; 1=Y, 2=FREEMAN_CODE]

	final static boolean BOUNDING_TOUCH_RESEARCH = true, NO_BOUNDING_TOUCH_RESEARCH = false;
	final static int X = 0, Y = 1, FREEMAN_CODE = 2;
	static int X_MIN, X_MAX, Y_MIN, Y_MAX;
	// EXtract Contours

	private int getValueO(double[] ip, int x, int y, int direction) {
		int value = -1;
		if (y == Y_MAX && (direction == 5 || direction == 6 || direction == 7))
			return value;
		if (y == Y_MIN && (direction == 1 || direction == 2 || direction == 3))
			return value;
		if (x == X_MIN && (direction == 3 || direction == 4 || direction == 5))
			return value;
		if (x == X_MAX && (direction == 7 || direction == 0 || direction == 1))
			return value;

		int w = X_MAX + 1;
		switch (direction) {
		case 0:
			value = ((int) ip[x + 1 + y * w]);
			break;
		case 1:
			value = ((int) ip[x + 1 + (y - 1) * w]);
			break;
		case 2:
			value = ((int) ip[x + (y - 1) * w]);
			break;
		case 3:
			value = ((int) ip[x - 1 + (y - 1) * w]);
			break;
		case 4:
			value = ((int) ip[x - 1 + y * w]);
			break;
		case 5:
			value = ((int) ip[x - 1 + (y + 1) * w]);
			break;
		case 6:
			value = ((int) ip[x + (y + 1) * w]);
			break;
		case 7:
			value = ((int) ip[x + 1 + (y + 1) * w]);
			break;
		default:
			break;
		}

		return value;
	}

	// Contour extraction label image from cc Sequence
	private void extractEdgeO(IcyBufferedImage vi, int nbcc) {

		int direction = 0;
		int greylevel, oldgl = nbcc + 1; // explore une label image
		int[][] edgetmp;
		// oldgl = 0;

		int w = vi.getWidth();
		int h = vi.getHeight();
		double[] tabIn = Array1DUtil.arrayToDoubleArray(vi.getDataXY(0), vi.isSignedDataType());

		X_MIN = 0;
		X_MAX = w - 1;
		Y_MIN = 0;
		Y_MAX = h - 1;
		edge = new ArrayList();
		System.out.println(" ");
		System.out.println("EXTRACT EDGE ");

		ArrayList<PointContour> contourtmp = new ArrayList<PointContour>();

		// TODO A optimiser a) verifier que tous les contours peuvent etre
		// extraits (interieurs)

		/*
		 * Pb avec les regions interieures for (int x=vi.getWidth()-1; x>=0;x--)
		 * for (int y=vi.getHeight()-1; y>=0;y--) {
		 */
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++) {
				// System.out.println( " x: "+x+" y: "+y+" oldgl:"+oldgl);
				greylevel = (int) tabIn[x + y * w];

				if (greylevel != 0 && greylevel < oldgl) {
					// if (greylevel!=0 && greylevel>oldgl){
					// System.out.println("greylevel= "+greylevel+ " x: "+x+" y:
					// "+y+" oldgl:"+oldgl);
					direction = 0;
					contourtmp = exploreEdgeO(w, tabIn, x, y, direction, greylevel);
					oldgl = greylevel;
					// System.out.println("greylevel "+greylevel+ " x: "+x+" y:
					// "+y+" oldgl:"+oldgl);
					if (contourtmp.size() > 2) {
						CntLabel ctlabel = new CntLabel();
						ctlabel.pts = contourtmp;
						ctlabel.label = greylevel;
						listContours.add(ctlabel);
					}
					// edge.add(edgetmp);
					// edge.add(greylevel, edgetmp); Creer une classe !!!
					// System.out.println("edgetmp.size "+edgetmp.length);
				}
			}

		System.out.println("End");
	}

	/*
	 * return the freeman code of the connected component begining by (x,y)
	 * initial pixel(the most top-left) The connected component is in greyLevel
	 */
	private ArrayList<PointContour> exploreEdgeO(int w, double[] ip, int x, int y, int direction, int greyLevel) {

		ArrayList<int[]> v = new ArrayList<>();
		ArrayList<PointContour> ct = new ArrayList<PointContour>();
		boolean directionFound;
		boolean validationDirectionFound = true;
		boolean firstPoint = true;
		int greyLevelTmp = 0;

		// System.out.println("exploreEdgeO "+ " x:"+x+" y:"+y);
		// System.out.println("greylevel:" +greyLevel +" ip.getPixelValue(x, y,
		// 0):"+ (int)ip.getPixelValue(x, y, 0));

		while ((int) (ip[x + y * w]) != greyLevel) {
			x++;
		}
		int xInit = x;
		int yInit = y;
		int xTmp = x;
		int yTmp = y;
		while (!(x == xInit && y == yInit && !firstPoint)) { // jusqu'a l'arrete
			// de la fin de
			// la recherche
			// de freeman
			xTmp = x;
			yTmp = y;

			// determination de la direction a explorer
			if (firstPoint) {
				if (x == X_MAX)
					direction = 6;
				else
					direction = 0;
				firstPoint = false;
			} else {
				if (validationDirectionFound)
					direction = (2 * (((int) ((direction + 1) / 2)) % 4)) + 1;
				else
					validationDirectionFound = true;
			}

			// recherche de l'index de freeman
			do {
				directionFound = false;
				if (getValueO(ip, x, y, direction) == greyLevel) {
					switch (direction) { // freemanIndex
					case 0:
						xTmp++;
						break;
					case 1:
						xTmp++;
						yTmp--;
						break;
					case 2:
						yTmp--;
						break;
					case 3:
						xTmp--;
						yTmp--;
						break;
					case 4:
						xTmp--;
						break;
					case 5:
						xTmp--;
						yTmp++;
						break;
					case 6:
						yTmp++;
						break;
					case 7:
						xTmp++;
						yTmp++;
						break;

					default:
						break;
					}

					directionFound = true;
				} else {
					direction = (direction + 7) % 8; // tourne dans le sens des
					// aiguille d'une montre
				}

			} while (!directionFound);

			// le code de freeman est trouve et est egale a parameters[2]
			// pour verifier qu'il n'y a pas une region "extracellulaire" a la
			// composante connexe qui serait invaginee par connexite 8
			if ((direction % 2) == 1) { // freeman impaire = cas des diagonales
				int directionTmp = (direction + 7) % 8;
				int xTmp2 = x;
				int yTmp2 = y;
				// greyLevelTmp=(greyLevel==0)?255:0;// couleur oppose

				if (getValueO(ip, xTmp2, yTmp2, directionTmp) != greyLevel) { // il
					// y
					// a
					// possibilite
					// de
					// poche!!!
					switch (directionTmp) { // freemanIndex
					case 0:
						xTmp2++;
						break;
					case 1:
						xTmp2++;
						yTmp2--;
						break;
					case 2:
						yTmp2--;
						break;
					case 3:
						xTmp2--;
						yTmp2--;
						break;
					case 4:
						xTmp2--;
						break;
					case 5:
						xTmp2--;
						yTmp2++;
						break;
					case 6:
						yTmp2++;
						break;
					case 7:
						xTmp2++;
						yTmp2++;
						break;
					default:
						break;
					}
					// validationDirectionFound = !
					// checkClosedBubble(ip,xTmp2,yTmp2,directionTmp,
					// greyLevelTmp);
					validationDirectionFound = true;
				}
			}

			// System.out.println("validation found :"+validationDirectionFound+
			// " greyLevelTmp:"+greyLevelTmp);

			if (validationDirectionFound) {
				// ip.putPixel(x,y,greyLevelTmp); // pour ne pas que le codage
				// repasse par ce pixel, il est re�mplace dans l'image
				// ip[x+y*w]= -1;
				x = xTmp;
				y = yTmp;
				int[] result = { x, y, direction };
				v.add(result);

				PointContour cnt = new PointContour();
				cnt.x = x;
				cnt.y = y;
				cnt.dir = direction;

				ct.add(cnt);

				// System.out.println("validation found :"+x+" "+y+" dir =
				// "+direction);
			} else { // TODO verifier le type image
				ip[x + y * w] = greyLevelTmp; // pour ne pas que le codage
				// repasse par ce pixel, il est
				// remplace dans l'image

				direction = (direction + 7) % 8;
				// System.out.println(greyLevelTmp + " found :"+x+" "+y+" dir =
				// "+direction);
			}
		}

		int[][] edget = new int[v.size()][3];
		for (int i = 0; i < v.size(); i++) {
			edget[i] = (int[]) v.get(i);
			// A quoi cela sert ?

		}

		// System.out.print("v.size 2 "+v.size());
		return ct; /// return keep ROI
	}//

	
	public static void main(String[] arg0)
	{
		Icy.main(arg0);
	
	}
}
